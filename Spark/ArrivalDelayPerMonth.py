'''
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Arrival Delay per Month
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
'''

from pyspark import SparkConf, SparkContext
import collections
import math

conf = SparkConf() \
	.setMaster("spark://master:7077") \
	.set("spark.submit.deployMode", "cluster") \
	.setAppName("ArrivalDelayPerMonth")
sc = SparkContext(conf = conf)

# gia kathe ptisi: (minas), (kathisterisi afiksis, 1)
# gia kathe mina: (minas), (athroisma kathisteriseon afikseon, athroisma ptiseon)
# gia kathe mina: (minas), (mesi kathisterisi afiksis)
avg = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda x: (int(x.split(",")[1]), (int(x.split(",")[14]), 1))) \
	.reduceByKey(lambda a,b: (a[0]+b[0], a[1]+b[1]))\
	.mapValues(lambda v: v[0]/v[1])

# gia kathe ptisi: (minas), (kathisterisi afiksis)
# gia kathe ptisi: (minas), (kathisterisi afiksis, mesi kathisterisi afiksis)
# gia kathe ptisi: (minas), (mesi kathsiterisi, (kathisterisi - mesi kathisterisi)^2, 1)
# gia kathe ptisi: (minas, mesi kathisterisi), ((kathisterisi - mesi kathisterisi)^2, 1)
# gia kathe mina: (minas, mesi kathisterisi), (SUM((kathisterisi - mesi kathisterisi)^2), athroisma ptiseon)
# gia kathe mina: (minas, mesi kathisterisi), (tipiki apoklisi)
# gia kathe mina: (minas), (mesi kathisterisi, tipiki apoklisi)
sd = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda x: (int(x.split(",")[1]), int(x.split(",")[14]))) \
        .join(avg) \
        .mapValues(lambda v: (v[1], (v[0] - v[1]) ** 2, 1)) \
        .map(lambda x: ((x[0], x[1][0]) , (x[1][1], x[1][2])) ) \
        .reduceByKey(lambda a,b: (a[0]+b[0], a[1]+b[1])) \
        .mapValues(lambda v: math.sqrt(v[0]/v[1])) \
	.map(lambda x: (x[0][0], (x[0][1], x[1])))

sResults = collections.OrderedDict(sorted(sd.collectAsMap().items()))

f = open("ArrivalDelayPerMonth.out","w")
f.write("Month\tAvg\tStdDev\n")
for key, value in sorted(sResults.items()):
        f.write("%d\t%f\t%f\n" % (key, value[0], value[1]))
f.close()
