'''
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : States Popularity
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
'''

from pyspark import SparkConf, SparkContext
import collections 

conf = SparkConf() \
	.setMaster("spark://master:7077") \
	.set("spark.submit.deployMode", "cluster") \
	.setAppName("StatesPopularity")
sc = SparkContext(conf = conf)

# gia kathe aerodromio: (aerodromio), (politeia)
airports = sc.textFile("hdfs://master:9000/user/hive/warehouse/airports") \
	.map(lambda line: (line.split(",")[0], line.split(",")[3]))

# gia kathe ptisi: (aerodromio proorismou), (1)
# gia kathe aerodromio: (aerodromio), (athroisma afikseon)
# gia kathe aerodromio: (aerodromio), (athroisma afikseon, politeia)
# gia kathe aerodromio: (politeia), (athroisma afikseon)
# gia kathe politeia: (politeia, athroisma afikseon)
states = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda line: (line.split(",")[17], 1)) \
	.reduceByKey(lambda a, b: a + b) \
	.join(airports) \
	.map(lambda x: (x[1][1], x[1][0])) \
	.reduceByKey(lambda a, b: a + b)

sResults = collections.OrderedDict(sorted(states.collectAsMap().items()))

f = open("StatesPopularity.out","w")
f.write("States\t#Arrivals\n")
for key, value in sorted(sResults.items()):
        f.write("%s\t%d\n" % (key, value))
f.close()
