'''
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Flights per Year
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
'''

from pyspark import SparkConf, SparkContext
import collections 

conf = SparkConf() \
	.setMaster("spark://master:7077") \
	.set("spark.submit.deployMode", "cluster") \
	.setAppName("FlightsPerYear")
sc = SparkContext(conf = conf)

# gia kathe ptisi: (xronia), (1)
# gia kathe xronia: (xronia), (athroisma ptiseon)
years = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda x: int(x.split(",")[0])) \
	.countByValue()

f = open("FlightsPerYear.out","w")
f.write("Year\t#Flights\n")
for key, value in sorted(years.items()):
	f.write("%d\t%d\n" % (key, value))
f.close()
