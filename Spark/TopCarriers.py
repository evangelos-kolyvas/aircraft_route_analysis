'''
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Top Carriers
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
'''

from pyspark import SparkConf, SparkContext
import collections 

conf = SparkConf() \
	.setMaster("spark://master:7077") \
	.set("spark.submit.deployMode", "cluster") \
	.setAppName("TopCarriers")
sc = SparkContext(conf = conf)

# gia kathe etaireia: (etaireia), (perigrafi)
carriers = sc.textFile("hdfs://master:9000/user/hive/warehouse/carriers") \
	.map(lambda line: (line.split(",")[0], line.split(",")[1]))

# gia kathe ptisi: (etaireia), (1)
# gia kathe etaireia: (etaireia), (athroisma ptiseon)
# top-10 dimofilesteres etaireies
topNList = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda line: (line.split(",")[8], 1)) \
	.reduceByKey(lambda a, b: a + b) \
	.top(10, key=lambda x: x[1])

topNRDD = sc.parallelize(topNList)

# gia kathe ptisi: (etaireia, xronia ptisis), (1)
# gia kathe etaireia, tin sigkekrimeni xronia: (etaireia, xronia) (athroisma ptiseon)
# gia kathe etaireia: (etaireia), (xronia, athroisma ptiseon tin sigkekrimeni xronia)
# gia kathe etaireia: (etaireia), [(xronia, ptiseis xronias), ..., (xronia, ptiseis xronias)]
# gia tis top-10 etaireies: (etaireia), ([(xronia, ptiseis xronias), ..., (xronia, ptiseis xronias)], geniko athroisma)
# gia tis top-10 etaireies: (etaireia), [([(xronia, ptiseis xronias), ..., (xronia, ptiseis xronias)], geniko athroisma), perigrafi]
# gia tis top-10 etaireies: (perigrafi), ([(xronia, ptiseis xronias), ..., (xronia, ptiseis xronias)], geniko athroisma)
topNStat = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda line: ((line.split(",")[8], int(line.split(",")[0])), 1)) \
	.reduceByKey(lambda a, b: a + b) \
	.map(lambda x: (x[0][0], (x[0][1], x[1]))) \
	.groupByKey().mapValues(list) \
	.join(topNRDD) \
	.join(carriers) \
	.map(lambda x: (x[1][1], x[1][0]))

sResults = collections.OrderedDict(sorted(topNStat.collectAsMap().items()))

f = open("TopCarriers.out","w")
f.write("Carriers\t([(year, yearSum), (year, yearSum), ..., (year, yearSum)], carrierSum)\n")
for key, value in sorted(sResults.items()):
        f.write("%s\t%s\n" % (key, value))
f.close()
