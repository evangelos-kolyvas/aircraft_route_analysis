'''
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Plane Age per Year
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
'''

from pyspark import SparkConf, SparkContext
import collections 

conf = SparkConf() \
	.setMaster("spark://master:7077") \
	.set("spark.submit.deployMode", "cluster") \
	.setAppName("PlaneAgePerYear")
sc = SparkContext(conf = conf)

# gia kathe aeroplano: (aeroplano), (xronia kataskeuis)
planes = sc.textFile("hdfs://master:9000/user/hive/warehouse/planes") \
	.map(lambda x: (x.split(",")[0], int(x.split(",")[8])))

# gia kathe ptisi: (aeroplano), (xronia ptisis)
# gia kathe ptisi: (aeroplano), (xronia ptisis, xronia kataskeuis)
# gia kathe xronia: (xronia), (ilikia aeroplanou, 1)
# gia kathe xronia: (xronia), (athroisma ilikias aeroplanon, athroisma aeroplanon)
# gia kathe xronia: (xronia), (mesi ilikia aeroplanon)
flights = sc.textFile("hdfs://master:9000/user/hive/warehouse/flights") \
	.map(lambda x: (x.split(",")[10], int(x.split(",")[0]))) \
        .join(planes) \
        .map(lambda x: (x[1][0], (x[1][0] - x[1][1], 1))) \
        .reduceByKey(lambda a, b: (a[0] + b[0], a[1] + b[1])) \
        .mapValues(lambda v: v[0] / v[1])

sResults = collections.OrderedDict(sorted(flights.collectAsMap().items()))

f = open("PlaneAgePerYear.out","w")
f.write("Year\tAvg_Plane_Age\n")
for key, value in sorted(sResults.items()):
        f.write("%d\t%f\n" % (key, value))
f.close()
