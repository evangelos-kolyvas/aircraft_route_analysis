/* ********************************************************************************
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Top Carriers
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 * ********************************************************************************/

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Stack;

public class TopCarriers {

    public static class TopCarriers_Mapper1A
            extends Mapper<Object, Text, Text, Text> {

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe ptisi: (etaireia), (xronia ptisis, "A")
            context.write(new Text(flightData[8]), new Text(flightData[0] + ",A"));
        }
    }

    public static class TopCarriers_Mapper1B
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe etaireia: (etaireia), (perigrafi, "B")
            context.write(new Text(flightData[0]), new Text(flightData[1] + ",B"));
        }
    }

    public static class TopCarriers_Reducer1
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            String[] parts;
            int[] counting_array = new int[23];
            String description = ""; // plires onoma etaireias

            // gia kathe etaireia:
            // metra poses ptiseis ekane tis xronies 1987 - 2008 k athroistika
            for (Text val : values) {

                parts = val.toString().split(",");

                if(parts[1].equals("A")) {
                    counting_array[Integer.parseInt(parts[0]) - 1987]++;
                    counting_array[22]++;
                }
                else {
                    description = parts[0];
                }
            }

            String out = Arrays.toString(counting_array);
            out = out.replace(", ",",");
            out = out.replace("[","");
            out = out.replace("]","");
            context.write(new Text(description), new Text(out));
        }
    }

    public static class TopCarriers_Mapper2
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            context.write(new Text("common key"), value);
        }
    }

    public static class TopCarriers_Reducer2
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            // sigkrine me vasi tin 23i thesi tou pinaka
            // 0    <-- plires onoma etaireias
            // 1-22 <-- athroisma ptiseon gia tis xronies 1987-2008 antistoixa
            // 23   <-- sinoliko athroisma
            PriorityQueue<String[]> pq = new PriorityQueue<>(Comparator.comparingInt(e -> Integer.parseInt(e[23])));
            String[] parts;

            for (Text val : values) {

                parts = val.toString().split(",");

                pq.add(parts);

                // remove minimum if k+1 entries on the pq
                if(pq.size() > 10)
                {
                    pq.poll();
                }
            }

            // print in descending order
            Stack<String[]> stack = new Stack<>();
            while(!pq.isEmpty()) {
                stack.push(pq.poll());
            }

            String out;
            while (!stack.isEmpty()) {
                parts = stack.pop();
                out = Arrays.toString(parts);
                out = out.replace(", ",",");
                out = out.substring(out.indexOf(",") + 1, out.length());
                out = out.replace("]","");
                context.write(new Text(parts[0]), new Text(out));
            }
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator",",");
        ///////////////////////////////////////////
        Job job1 = Job.getInstance(conf, "TopCarriers1");
        job1.setJarByClass(TopCarriers.class);
        //job1.setMapperClass();
        //job1.setCombinerClass();
        job1.setReducerClass(TopCarriers_Reducer1.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);

        MultipleInputs.addInputPath(job1, new Path("/user/hive/warehouse/flights"),
                TextInputFormat.class, TopCarriers_Mapper1A.class);
        MultipleInputs.addInputPath(job1, new Path("/user/hive/warehouse/carriers"),
                TextInputFormat.class, TopCarriers_Mapper1B.class);
        FileOutputFormat.setOutputPath(job1, new Path("/MR_outputs/TopCarriers/out1"));
        ///////////////////////////////////////////
        job1.waitForCompletion(true);
        ///////////////////////////////////////////
        Job job2 = Job.getInstance(conf, "TopCarriers2");
        job2.setJarByClass(TopCarriers.class);
        job2.setMapperClass(TopCarriers_Mapper2.class);
        //job2.setCombinerClass();
        job2.setReducerClass(TopCarriers_Reducer2.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job2, new Path("/MR_outputs/TopCarriers/out1/part*"));
        FileOutputFormat.setOutputPath(job2, new Path("/MR_outputs/TopCarriers/out2"));
        ///////////////////////////////////////////
        System.exit(job2.waitForCompletion(true) ? 0 : 1);
    }
}
