/* ********************************************************************************
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Plane Age per Year
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 * ********************************************************************************/

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class PlaneAgePerYear {

    public static class PlaneAgePerYear_Mapper1A
            extends Mapper<Object, Text, Text, Text> {

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe ptisi: (aeroplano), (xronia ptisis, "A")
            context.write(new Text(flightData[10]), new Text(flightData[0] + ",A"));
        }
    }

    public static class PlaneAgePerYear_Mapper1B
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe aeroplano: (aeroplano), (xronia kataskeuis, "B")
            context.write(new Text(flightData[0]), new Text(flightData[8] + ",B"));
        }
    }

    public static class PlaneAgePerYear_Reducer1
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            String[] parts;

            // gia kathe aeroplano:
            int[] counting_array = new int[22]; // metra poses ptiseis ekane tis xronies 1987 - 2008
            int plane_year = 0; // xronia kataskeuis

            for (Text val : values) {

                parts = val.toString().split(",");

                if(parts[1].equals("A")) {
                    counting_array[Integer.parseInt(parts[0]) - 1987]++;
                }
                else {
                    plane_year = Integer.parseInt(parts[0]);
                }

            }

            // gia kathe aeroplano, gia oles tis xronies:
            int year;   // xronia ptisis
            int age;    // ilikia aeroplanou
            for (int i = 0; i < 22; i++) {
                if (counting_array[i] > 0) // an petakse tin sigkekrimeni xronia:
                {
                    year = i + 1987;
                    age = year - plane_year;
                    // (xronia ptisis), (ilikia aeroplanou, poses ptiseis ekane tin sigkekrimeni xronia)
                    context.write(new Text("" + year), new Text(age + "," + counting_array[i]));
                }
            }
        }
    }

    public static class PlaneAgePerYear_Mapper2
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // (xronia ptisis), (ilikia aeroplanou, poses ptiseis ekane tin sigkekrimeni xronia)
            context.write(new Text(flightData[0]), new Text(flightData[1] + "," + flightData[2]));
        }
    }

    public static class PlaneAgePerYear_Reducer2
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            String[] parts;
            int age_counter = 0;
            int flights_counter = 0;

            // gia kathe aeroplano pou petakse tin sigkekrimeni xronia
            int age;        // ilikia aeroplanou
            int flights;    // arithmos ptiseon
            for (Text val : values) {

                parts = val.toString().split(",");
                age = Integer.parseInt(parts[0]);
                flights = Integer.parseInt(parts[1]);

                age_counter += age * flights;
                flights_counter += flights;
            }

            // (xronia), (mesi ilikia aeroplanon tin sigkekrimeni xronia)
            context.write(key, new Text("" + (double) age_counter / flights_counter));
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator",",");
        ///////////////////////////////////////////
        Job job1 = Job.getInstance(conf, "PlaneAgePerYear1");
        job1.setJarByClass(PlaneAgePerYear.class);
        //job1.setMapperClass();
        //job1.setCombinerClass();
        job1.setReducerClass(PlaneAgePerYear_Reducer1.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);

        MultipleInputs.addInputPath(job1, new Path("/user/hive/warehouse/flights"),
                TextInputFormat.class, PlaneAgePerYear_Mapper1A.class);
        MultipleInputs.addInputPath(job1, new Path("/user/hive/warehouse/planes"),
                TextInputFormat.class, PlaneAgePerYear_Mapper1B.class);
        FileOutputFormat.setOutputPath(job1, new Path("/MR_outputs/PlaneAgePerYear/out1"));
        ///////////////////////////////////////////
        job1.waitForCompletion(true);
        ///////////////////////////////////////////
        Job job2 = Job.getInstance(conf, "PlaneAgePerYear2");
        job2.setJarByClass(PlaneAgePerYear.class);
        job2.setMapperClass(PlaneAgePerYear_Mapper2.class);
        //job2.setCombinerClass();
        job2.setReducerClass(PlaneAgePerYear_Reducer2.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job2, new Path("/MR_outputs/PlaneAgePerYear/out1/part*"));
        FileOutputFormat.setOutputPath(job2, new Path("/MR_outputs/PlaneAgePerYear/out2"));
        ///////////////////////////////////////////
        System.exit(job2.waitForCompletion(true) ? 0 : 1);
    }
}
