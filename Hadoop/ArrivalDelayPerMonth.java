/* ********************************************************************************
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Arrival Delay per Month
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 * ********************************************************************************/

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class ArrivalDelayPerMonth {

    public static class ArrivalDelayPerMonth_Mapper1
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe ptisi: (minas), (kathisterisi afiksis)
            context.write(new Text(flightData[1]), new Text(flightData[14]));
        }
    }

    public static class ArrivalDelayPerMonth_Combiner1
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            int sum = 0;
            int counter = 0;

            for (Text val : values) {
                sum += Integer.parseInt(val.toString());
                counter++;
            }

            // gia kathe mina: (minas), (athroisma kathisteriseon afikseon, athroisma ptiseon)
            context.write(key, new Text(sum + "," + counter));
        }
    }

    public static class ArrivalDelayPerMonth_Reducer1
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            String[] combinerOutput;
            int sum = 0;
            int counter = 0;

            for (Text val : values) {
                combinerOutput = val.toString().split(",");
                sum += Integer.parseInt(combinerOutput[0]);
                counter += Integer.parseInt(combinerOutput[1]);
            }

            // gia kathe mina: (minas), (mesi kathisterisi afiksis)
            context.write(key, new Text("" + (double) sum/counter));
        }
    }

    public static class ArrivalDelayPerMonth_Mapper2A
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe mina: (minas), (mesi kathisterisi afiksis, "A")
            context.write(new Text(flightData[0]), new Text(flightData[1] + ",A"));
        }
    }

    public static class ArrivalDelayPerMonth_Mapper2B
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe ptisi: (minas), (kathisterisi afiksis, "B")
            context.write(new Text(flightData[1]), new Text(flightData[14] + ",B"));
        }
    }

    public static class ArrivalDelayPerMonth_Reducer2
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            String[] parts;
            double avgArrDelay = 0; // mesi kathisterisi afiksis ton sigkekrimeno mina
            int arrDelay; // sigkekrimeni timi kathisterisis afiksis
            int times; // poses fores paratireitai i sigkekrimeni kathisterisi
            HashMap<Integer, Integer> delaysCounter = new HashMap<>(); // (arrDelay, times)
            int N = 0; // plithos ptiseon ton sigkekrimeno mina
            double stdDev = 0; // tipiki apoklisi

            for (Text val : values) {

                parts = val.toString().split(",");

                if(parts[1].equals("A")) {
                    avgArrDelay = Double.parseDouble(parts[0]);
                }
                else {
                    arrDelay = Integer.parseInt(parts[0]);

                    if (delaysCounter.containsKey(arrDelay)) {
                        times = delaysCounter.get(arrDelay) + 1;
                        delaysCounter.put(arrDelay, times);
                    }
                    else {
                        delaysCounter.put(arrDelay, 1);
                    }
                }
            }

            for (Map.Entry<Integer, Integer> entry : delaysCounter.entrySet()) {
                arrDelay = entry.getKey();
                times = entry.getValue();
                N += times;
                stdDev += Math.pow(arrDelay - avgArrDelay, 2) * times;
            }
            stdDev = Math.sqrt(stdDev / N);

            // gia kathe mina: (minas), (mesi kathisterisi afiksis, tipiki apoklisi)
            context.write(key, new Text(avgArrDelay + "," + stdDev));
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator",",");
        ///////////////////////////////////////////
        Job job1 = Job.getInstance(conf, "ArrivalDelayPerMonth1");
        job1.setJarByClass(ArrivalDelayPerMonth.class);
        job1.setMapperClass(ArrivalDelayPerMonth_Mapper1.class);
        job1.setCombinerClass(ArrivalDelayPerMonth_Combiner1.class);
        job1.setReducerClass(ArrivalDelayPerMonth_Reducer1.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(job1, new Path("/user/hive/warehouse/flights"));
        FileOutputFormat.setOutputPath(job1, new Path("/MR_outputs/ArrivalDelayPerMonth/out1"));
        ///////////////////////////////////////////
        job1.waitForCompletion(true);
        ///////////////////////////////////////////
        Job job2 = Job.getInstance(conf, "ArrivalDelayPerMonth2");
        job2.setJarByClass(ArrivalDelayPerMonth.class);
        //job2.setMapperClass();
        //job2.setCombinerClass();
        job2.setReducerClass(ArrivalDelayPerMonth_Reducer2.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(Text.class);

        MultipleInputs.addInputPath(job2, new Path("/MR_outputs/ArrivalDelayPerMonth/out1/part*"),
                TextInputFormat.class, ArrivalDelayPerMonth_Mapper2A.class);
        MultipleInputs.addInputPath(job2, new Path("/user/hive/warehouse/flights"),
                TextInputFormat.class, ArrivalDelayPerMonth_Mapper2B.class);
        FileOutputFormat.setOutputPath(job2, new Path("/MR_outputs/ArrivalDelayPerMonth/out2"));
        ///////////////////////////////////////////
        System.exit(job2.waitForCompletion(true) ? 0 : 1);
    }
}
