/* ********************************************************************************
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : States Popularity
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 * ********************************************************************************/

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class StatesPopularity {

    public static class StatesPopularity_Mapper1A
            extends Mapper<Object, Text, Text, Text> {

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe ptisi: (aerodromio proorismou), (dummy value)
            context.write(new Text(flightData[17]), new Text("dummy value"));
        }
    }

    public static class StatesPopularity_Mapper1B
            extends Mapper<Object, Text, Text, Text>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe aerodromio: (aerodromio), (politeia)
            context.write(new Text(flightData[0]), new Text(flightData[3]));
        }
    }

    public static class StatesPopularity_Reducer1
            extends Reducer<Text,Text,Text,Text> {

        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            int counter = 0;
            String state = ""; // politeia pou vrisketai to aerodromio

            // gia kathe aerodromio:
            // metra poses ptiseis dexetai
            for (Text val : values) {

                if(val.toString().equals("dummy value")){
                    counter++;
                }
                else {
                    state = val.toString();
                }

            }

            // gia kathe aerodromio: (politeia), (ptiseis dexetai)
            context.write(new Text(state), new Text("" + counter));
        }
    }

    public static class StatesPopularity_Mapper2
            extends Mapper<Object, Text, Text, IntWritable>{

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            context.write(new Text(flightData[0]), new IntWritable(Integer.parseInt(flightData[1])));
        }
    }

    public static class StatesPopularity_Reducer2
            extends Reducer<Text,IntWritable,Text,IntWritable> {

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {

            int sum = 0;

            for (IntWritable val : values) {
                sum += val.get();
            }

            // gia kathe politeia: (politeia), (ptiseis dexetai)
            context.write(key, new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator",",");
        ///////////////////////////////////////////
        Job job1 = Job.getInstance(conf, "StatesPopularity1");
        job1.setJarByClass(StatesPopularity.class);
        //job1.setMapperClass();
        //job1.setCombinerClass();
        job1.setReducerClass(StatesPopularity_Reducer1.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);

        MultipleInputs.addInputPath(job1, new Path("/user/hive/warehouse/flights"),
                TextInputFormat.class, StatesPopularity_Mapper1A.class);
        MultipleInputs.addInputPath(job1, new Path("/user/hive/warehouse/airports"),
                TextInputFormat.class, StatesPopularity_Mapper1B.class);
        FileOutputFormat.setOutputPath(job1, new Path("/MR_outputs/StatesPopularity/out1"));
        ///////////////////////////////////////////
        job1.waitForCompletion(true);
        ///////////////////////////////////////////
        Job job2 = Job.getInstance(conf, "StatesPopularity2");
        job2.setJarByClass(StatesPopularity.class);
        job2.setMapperClass(StatesPopularity_Mapper2.class);
        //job2.setCombinerClass();
        job2.setReducerClass(StatesPopularity_Reducer2.class);
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(IntWritable.class);

        FileInputFormat.setInputPaths(job2, new Path("/MR_outputs/StatesPopularity/out1/part*"));
        FileOutputFormat.setOutputPath(job2, new Path("/MR_outputs/StatesPopularity/out2"));
        ///////////////////////////////////////////
        System.exit(job2.waitForCompletion(true) ? 0 : 1);
    }
}
