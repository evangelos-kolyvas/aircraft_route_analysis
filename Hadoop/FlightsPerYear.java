/* ********************************************************************************
 * Project     : Big Data Management - Spring 2018
 * Authors     : Kolyvas Evangelos (M1595) - Polydouri Andrianna (M1598)
 * Date        : June 2, 2018
 * Description : Flights per Year
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 * ********************************************************************************/

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class FlightsPerYear {

    public static class FlightsPerYear_Mapper
            extends Mapper<Object, Text, Text, IntWritable> {

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

            String[] flightData = value.toString().split(",");

            // gia kathe ptisi: (xronia), (1)
            context.write(new Text(flightData[0]), new IntWritable(1));
        }
    }

    public static class FlightsPerYear_Reducer
            extends Reducer<Text,IntWritable,Text,IntWritable> {

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {

            int sum = 0;

            for (IntWritable val : values) {
                sum += val.get();
            }

            // gia kathe xronia: (xronia), (athroisma ptiseon)
            context.write(key, new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("mapreduce.output.textoutputformat.separator",",");
        ///////////////////////////////////////////
        Job job = Job.getInstance(conf, "FlightsPerYear");
        job.setJarByClass(FlightsPerYear.class);
        job.setMapperClass(FlightsPerYear_Mapper.class);
        job.setCombinerClass(FlightsPerYear_Reducer.class);
        job.setReducerClass(FlightsPerYear_Reducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path("/user/hive/warehouse/flights"));
        FileOutputFormat.setOutputPath(job, new Path("/MR_outputs/FlightsPerYear"));
        ///////////////////////////////////////////
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
